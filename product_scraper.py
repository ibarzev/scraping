import time
from selenium import webdriver
from bs4 import BeautifulSoup
import json

#Navigating to the web page using Selenium
driver = webdriver.Chrome()
driver.get('https://shop.mango.com/bg-en/women/skirts-midi/midi-satin-skirt_17042020.html?c=99');
time.sleep(5)

#Pulling the data with Beautiful Soup
page_source = driver.page_source
soup = BeautifulSoup(page_source, 'lxml')

#Scraping data from the product
scrape_name = soup.find("h1", {"class": "product-name"})
scrape_price = soup.find("span", {"class": "product-sale product-sale--discount"})
scrape_color = soup.find("span", {"class": "colors-info-name"})
scrape_size = soup.find_all("div", {"class": "selector-list"})

#Preparing the scraped data for formatting
name = str(scrape_name.getText())
price = str(scrape_price.getText())
price = float(price.replace('лв.',''))
color = str(scrape_color.getText())
size = str(scrape_size)

#Getting every of the available and unavailable product size from list of possible sizes for that caterogy of products
possible_sizes = ['XXS','XS', 'S', 'M', 'L', 'XL', 'XXL']
product_sizes = []
for siz in possible_sizes:
    if size.find(siz) > -1:
        product_sizes.append(siz)

#Creating dictionary with scraped data after formatting
product_data = {
  "name": name,
  "price": price,
  "color": color,
  "size": product_sizes
}

#Converting dictionary to JSON object
product_json = json.dumps(product_data, indent = 4)

# Writing to product.json
with open("product.json", "w") as outfile:
    outfile.write(product_json)

time.sleep(10)

driver.quit()