# About the task #

The goal of this task is to retrieve the information about a product on a given web page.
The information to be gathered is about name, color, price and size.
The output is saved as JSON file named product.json.
